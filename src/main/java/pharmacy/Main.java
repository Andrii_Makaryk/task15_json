package pharmacy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;

public class Main {
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    public static void main(String[] args) {
        String path="E:\\task14_JSON\\src\\main\\resources\\medicine.json";
       Medicine m=new Medicine("Diclofenac","Vishfa","Nesteroid", Arrays.asList("Dicloberl","Diclobru","Evinopon"),Arrays.asList("Solution","Tablets"),new Certificate(123456,"12/04/2018","12/04/2023","MOZ"),new Dosage(3.0,"twice"));
        String json = GSON.toJson(m);
        System.out.println(json);

        Medicine medicineSecond=GSON.fromJson(json,Medicine.class);
        System.out.println(medicineSecond.toString());

    }

}