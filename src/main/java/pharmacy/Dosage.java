package pharmacy;

public class Dosage {
    private double dose;
    private String frequency;

    public Dosage() {}

    public Dosage(double dose, String frequency) {
        this.dose = dose;
        this.frequency = frequency;
    }

    public double getDosage() {
        return dose;
    }

    public void setDosage(double dose) {
        this.dose = dose;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return "Dosage [dosage=" + dose + ", frequency=" + frequency + "]";
    }
}